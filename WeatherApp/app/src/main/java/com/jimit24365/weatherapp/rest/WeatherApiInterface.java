package com.jimit24365.weatherapp.rest;


import com.jimit24365.weatherapp.model.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jimit on 23/10/16.
 */
public interface WeatherApiInterface {

    @GET("yql")
    Call<WeatherResponse> getWeatherForecast(@Query("q") String q, @Query("format") String dataFormat, @Query("env") String env);
}
