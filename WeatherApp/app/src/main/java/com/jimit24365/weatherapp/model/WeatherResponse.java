package com.jimit24365.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jimit on 23/10/16.
 */
public class WeatherResponse {
    @SerializedName("query")
    private WeatherResponseResults weatherResponseResultsList;


    public WeatherResponseResults getWeatherResponseResultsList() {
        return weatherResponseResultsList;
    }

    public void setWeatherResponseResultsList(WeatherResponseResults weatherResponseResultsList) {
        this.weatherResponseResultsList = weatherResponseResultsList;
    }
}
