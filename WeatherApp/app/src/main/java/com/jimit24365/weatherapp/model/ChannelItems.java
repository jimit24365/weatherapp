package com.jimit24365.weatherapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jimit on 23/10/16.
 */
public class ChannelItems {

    @SerializedName("forecast")
    private List<WeatherDetails> weatherDetailsArrayList;

    public List<WeatherDetails> getWeatherDetailsArrayList() {
        return weatherDetailsArrayList;
    }

    public void setWeatherDetailsArrayList(List<WeatherDetails> weatherDetailsArrayList) {
        this.weatherDetailsArrayList = weatherDetailsArrayList;
    }
}
