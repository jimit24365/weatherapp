package com.jimit24365.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jimit on 23/10/16.
 */
public class WeatherResponseResults {
    @SerializedName("count")
    private int count;
    @SerializedName("created")
    private String created;
    @SerializedName("lang")
    private String lang;
    @SerializedName("results")
    private WeatherResultsModel weatherResultsModel;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public WeatherResultsModel getWeatherResultsModel() {
        return weatherResultsModel;
    }

    public void setWeatherResultsModel(WeatherResultsModel weatherResultsModel) {
        this.weatherResultsModel = weatherResultsModel;
    }
}
