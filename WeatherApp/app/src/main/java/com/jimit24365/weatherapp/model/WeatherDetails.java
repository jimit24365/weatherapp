package com.jimit24365.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jimit on 23/10/16.
 */
public class WeatherDetails {
    @SerializedName("code")
    private String code;
    @SerializedName("date")
    private String date;
    @SerializedName("day")
    private String day;
    @SerializedName("high")
    private String high;
    @SerializedName("low")
    private String low;
    @SerializedName("text")
    private String text;

    public WeatherDetails(String code,String date,String day,String high,String low,String text) {
        this.code = code;
        this.date = date;
        this.day = day;
        this.high = high;
        this.low = low;
        this.text = text;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
