package com.jimit24365.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jimit on 23/10/16.
 */
public class WeatherChannels {

    @SerializedName("item")
    private ChannelItems channelItems;

    public ChannelItems getChannelItems() {
        return channelItems;
    }

    public void setChannelItems(ChannelItems channelItems) {
        this.channelItems = channelItems;
    }
}
