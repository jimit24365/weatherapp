package com.jimit24365.weatherapp.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.jimit24365.weatherapp.R;
import com.jimit24365.weatherapp.model.ChannelItems;
import com.jimit24365.weatherapp.model.WeatherChannels;
import com.jimit24365.weatherapp.model.WeatherDetails;
import com.jimit24365.weatherapp.model.WeatherResponse;
import com.jimit24365.weatherapp.model.WeatherResponseResults;
import com.jimit24365.weatherapp.model.WeatherResultsModel;
import com.jimit24365.weatherapp.rest.WeatherApiClient;
import com.jimit24365.weatherapp.rest.WeatherApiInterface;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherListActivity extends AppCompatActivity {

    private final static String TAG =  WeatherListActivity.class.getName();

    private RecyclerView mWeatherListContainerRcv;
    private TextView mErrorMessageTv;
    private AVLoadingIndicatorView mLoadingIndicatorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mWeatherListContainerRcv = (RecyclerView) findViewById(R.id.weather_list);
        mErrorMessageTv = (TextView) findViewById(R.id.no_data);
        mLoadingIndicatorView = (AVLoadingIndicatorView) findViewById(R.id.loading_indicator);
        mWeatherListContainerRcv.setLayoutManager(
                new LinearLayoutManager(WeatherListActivity.this));
        mWeatherListContainerRcv.setVisibility(View.INVISIBLE);
        mErrorMessageTv.setVisibility(View.INVISIBLE);
        fetchAndPopulateWeatherDetails();
    }

    /**
     * FetchData using retrofit library.
     */
    private void fetchAndPopulateWeatherDetails(){

        WeatherApiInterface apiService =
                WeatherApiClient.getClient().create(WeatherApiInterface.class);

        String urlQuery = WeatherListActivity.this.getResources().getString(R.string.query);
        String dataFormat = WeatherListActivity.this.getResources().getString(R.string.query_type);
        String env = WeatherListActivity.this.getResources().getString(R.string.query_enviroment);
        mLoadingIndicatorView.show();
        Call<WeatherResponse> call = apiService.getWeatherForecast(urlQuery,dataFormat,env);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                WeatherResponse weatherResponse = response.body();
                WeatherResponseResults weatherResponseResults = (WeatherResponseResults) weatherResponse.getWeatherResponseResultsList();
                WeatherResultsModel weatherResultsModel = weatherResponseResults.getWeatherResultsModel();
                WeatherChannels weatherChannels = weatherResultsModel.getChannels();
                ChannelItems channelItems = weatherChannels.getChannelItems();
                ArrayList<WeatherDetails> weatherDetailsList = new ArrayList<WeatherDetails>(channelItems.getWeatherDetailsArrayList());
                mWeatherListContainerRcv.setAdapter(new WeatherRecyclerViewAdapter(WeatherListActivity.this,weatherDetailsList));
                mErrorMessageTv.setVisibility(View.GONE);
                mWeatherListContainerRcv.setVisibility(View.VISIBLE);
                mLoadingIndicatorView.hide();
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                String message = WeatherListActivity.this.getResources().getString(R.string.network_failure);
                mWeatherListContainerRcv.setVisibility(View.INVISIBLE);
                mErrorMessageTv.setVisibility(View.VISIBLE);
                Snackbar.make(mWeatherListContainerRcv,message
                        ,Snackbar.LENGTH_SHORT).show();
                mLoadingIndicatorView.hide();
            }
        });
    }
}
