package com.jimit24365.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jimit on 23/10/16.
 */
public class WeatherResultsModel {

    @SerializedName("channel")
    private WeatherChannels channels;

    public WeatherChannels getChannels() {
        return channels;
    }

    public void setChannels(WeatherChannels channels) {
        this.channels = channels;
    }
}
