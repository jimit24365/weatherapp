package com.jimit24365.weatherapp.activity;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jimit24365.weatherapp.R;
import com.jimit24365.weatherapp.model.WeatherDetails;

import java.util.ArrayList;

/**
 * Created by jimit on 23/10/16.
 */

public class WeatherRecyclerViewAdapter extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.WeatherViewHolder>{

    Context mContext;
    ArrayList<WeatherDetails> weatherList;

    WeatherRecyclerViewAdapter(Activity context,ArrayList<WeatherDetails> weatherList){
        this.mContext = context;
        this.weatherList = weatherList;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View weatherView = inflater.inflate(R.layout.weather_list_item_layout, null);
        WeatherViewHolder vh =
                new WeatherViewHolder(weatherView);
        return vh;
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        WeatherDetails currentWeatherData = weatherList.get(position);
        holder.populateData(currentWeatherData);
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder {
        private ImageView weatherImage;
        private TextView weatherDate;
        private TextView weatherType;
        private TextView temperatureTv;

        public WeatherViewHolder(View itemView) {
            super(itemView);
            weatherImage = (ImageView) itemView.findViewById(R.id.weather_img);
            weatherDate = (TextView) itemView.findViewById(R.id.weather_date);
            weatherType = (TextView) itemView.findViewById(R.id.weather_type);
            temperatureTv = (TextView) itemView.findViewById(R.id.weather_temp);
        }

        public void populateData(WeatherDetails weather){
            if (weather != null) {

                if(!TextUtils.isEmpty(weather.getDate())){
                    weatherDate.setText(weather.getDate());
                }
                if(!TextUtils.isEmpty(weather.getText())){
                    weatherType.setText(weather.getText());
                }
                if(!TextUtils.isEmpty(weather.getHigh()) && !TextUtils.isEmpty(weather.getLow())){
                    String weatherTemperatureFormat = mContext.getResources().getString(R.string.formatted_high_low_temp);
                    String temperature = String.format(weatherTemperatureFormat,weather.getLow(),weather.getHigh());
                    temperatureTv.setText(temperature);
                }

                if(weather.getText().toLowerCase().contains("sun")){
                    weatherImage.setImageResource(R.drawable.sunny);
                }

                if(weather.getText().toLowerCase().contains("rain") || weather.getText().toLowerCase().contains("show") ){
                    weatherImage.setImageResource(R.drawable.raniy);
                }

                if(weather.getText().toLowerCase().contains("cloud")){
                    weatherImage.setImageResource(R.drawable.partly_cloudly);
                }
            }
        }
    }
}
